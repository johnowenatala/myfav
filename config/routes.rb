Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'favorite_categories#index'

  devise_for :users

  resources :favorite_categories do
    resources :favorites

  end

  get 'find_by_order' => 'favorite_categories#find_by_order', as: 'find_by_order'
end

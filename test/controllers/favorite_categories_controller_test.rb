require 'test_helper'

class FavoriteCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @favorite_category = favorite_categories(:one)
  end

  test "should get index" do
    get favorite_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_favorite_category_url
    assert_response :success
  end

  test "should create favorite_category" do
    assert_difference('FavoriteCategory.count') do
      post favorite_categories_url, params: { favorite_category: { description: @favorite_category.description, name: @favorite_category.name, order: @favorite_category.order, user_id: @favorite_category.user_id } }
    end

    assert_redirected_to favorite_category_url(FavoriteCategory.last)
  end

  test "should show favorite_category" do
    get favorite_category_url(@favorite_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_favorite_category_url(@favorite_category)
    assert_response :success
  end

  test "should update favorite_category" do
    patch favorite_category_url(@favorite_category), params: { favorite_category: { description: @favorite_category.description, name: @favorite_category.name, order: @favorite_category.order, user_id: @favorite_category.user_id } }
    assert_redirected_to favorite_category_url(@favorite_category)
  end

  test "should destroy favorite_category" do
    assert_difference('FavoriteCategory.count', -1) do
      delete favorite_category_url(@favorite_category)
    end

    assert_redirected_to favorite_categories_url
  end
end

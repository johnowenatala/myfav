# == Schema Information
#
# Table name: favorite_categories
#
#  id          :integer          not null, primary key
#  user_id     :integer          not null
#  order       :integer          not null
#  name        :string           not null
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_favorite_categories_on_user_id  (user_id)
#

class FavoriteCategory < ApplicationRecord
  belongs_to :user
  has_many :favorites, dependent: :destroy
  has_many :comments, dependent: :destroy

  scope :ordered, -> { order(:order) }
end

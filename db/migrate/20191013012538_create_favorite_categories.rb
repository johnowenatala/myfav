class CreateFavoriteCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :favorite_categories do |t|
      t.references :user, foreign_key: true, null: false
      t.integer :order, null: false
      t.string :name, null: false
      t.string :description

      t.timestamps
    end
  end
end

json.extract! favorite, :id, :favorite_category_id, :name, :author, :year, :url, :details, :destription, :created_at, :updated_at
json.url favorite_category_favorites_url(@favorite_category, favorite, format: :json)

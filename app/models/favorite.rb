# == Schema Information
#
# Table name: favorites
#
#  id                   :integer          not null, primary key
#  favorite_category_id :integer          not null
#  name                 :string           not null
#  author               :string
#  year                 :integer
#  url                  :string
#  details              :string
#  destription          :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_favorites_on_favorite_category_id  (favorite_category_id)
#

class Favorite < ApplicationRecord
  belongs_to :favorite_category
end

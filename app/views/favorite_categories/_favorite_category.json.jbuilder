json.extract! favorite_category, :id, :user_id, :order, :name, :description, :created_at, :updated_at
json.url favorite_category_url(favorite_category, format: :json)

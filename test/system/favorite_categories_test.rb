require "application_system_test_case"

class FavoriteCategoriesTest < ApplicationSystemTestCase
  setup do
    @favorite_category = favorite_categories(:one)
  end

  test "visiting the index" do
    visit favorite_categories_url
    assert_selector "h1", text: "Favorite Categories"
  end

  test "creating a Favorite category" do
    visit favorite_categories_url
    click_on "New Favorite Category"

    fill_in "Description", with: @favorite_category.description
    fill_in "Name", with: @favorite_category.name
    fill_in "Order", with: @favorite_category.order
    fill_in "User", with: @favorite_category.user_id
    click_on "Create Favorite category"

    assert_text "Favorite category was successfully created"
    click_on "Back"
  end

  test "updating a Favorite category" do
    visit favorite_categories_url
    click_on "Edit", match: :first

    fill_in "Description", with: @favorite_category.description
    fill_in "Name", with: @favorite_category.name
    fill_in "Order", with: @favorite_category.order
    fill_in "User", with: @favorite_category.user_id
    click_on "Update Favorite category"

    assert_text "Favorite category was successfully updated"
    click_on "Back"
  end

  test "destroying a Favorite category" do
    visit favorite_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Favorite category was successfully destroyed"
  end
end

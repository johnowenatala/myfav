$(document).on('turbolinks:load', function(){

  if ($('#devise_sessions_new').length > 0) {

    var form = $('#new_user');
    var submitHandler = function(submitEvent){

      var emailField = $('#user_email');
      var emailErrorMessageElement = $('#email_error_message');

      console.log("verificando valor en campo de email (con jQuery):", emailField.val());

      if (! emailField.val()) {
        submitEvent.preventDefault();
        if (emailErrorMessageElement.length == 0) {
          emailField.after('<div class="form-text text-danger" id="email_error_message"></div>');
          emailErrorMessageElement = $('#email_error_message');
        }
        emailField.addClass('is-invalid');
        emailField.removeClass('is-valid');
        emailErrorMessageElement.removeClass('d-none');
        emailErrorMessageElement.text('Inserta un correo para continuar');
      } else {
        emailErrorMessageElement.addClass('d-none');
        emailField.removeClass('is-invalid');
        emailField.addClass('is-valid');
      }

      var passwordField = $('#user_password');
      var passwordErrorMessageElement = $('#password_error_message');

      console.log("verificando valor en campo de password (con jQuery):", passwordField.val());

      if (! passwordField.val()) {
        submitEvent.preventDefault();
        if (passwordErrorMessageElement.length == 0) {
          passwordField.after('<div class="form-text text-danger" id="password_error_message"></div>');
          passwordErrorMessageElement = $('#password_error_message');
        }
        passwordField.addClass('is-invalid');
        passwordField.removeClass('is-valid');
        passwordErrorMessageElement.removeClass('d-none');
        passwordErrorMessageElement.text('Debes ingresar una contraseña');
      } else {
        passwordErrorMessageElement.addClass('d-none');
        passwordField.removeClass('is-invalid');
        passwordField.addClass('is-valid');
      }

      submitEvent.stopPropagation();

    };

    form.on('submit',submitHandler);

  }

});

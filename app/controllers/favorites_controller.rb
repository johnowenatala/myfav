class FavoritesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_favorite_category
  before_action :set_favorite, only: [:show, :edit, :update, :destroy]

  # GET /favorites
  # GET /favorites.json
  def index
    @favorites = @favorite_category.favorites
  end

  # GET /favorites/1
  # GET /favorites/1.json
  def show
  end

  # GET /favorites/new
  def new
    @favorite = @favorite_category.favorites.build
  end

  # GET /favorites/1/edit
  def edit
  end

  # POST /favorites
  # POST /favorites.json
  def create
    @favorite = @favorite_category.favorites.build(favorite_params)

    respond_to do |format|
      if @favorite.save
        format.html { redirect_to favorite_category_favorite_path(@favorite_category,@favorite), notice: 'Favorite was successfully created.' }
        format.json { render :show, status: :created, location: favorite_category_favorite_path(@favorite_category,@favorite) }
      else
        format.html { render :new }
        format.json { render json: @favorite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /favorites/1
  # PATCH/PUT /favorites/1.json
  def update
    respond_to do |format|
      if @favorite.update(favorite_params)
        format.html { redirect_to favorite_category_favorite_path(@favorite_category,@favorite), notice: 'Favorite was successfully updated.' }
        format.json { render :show, status: :ok, location: favorite_category_favorite_path(@favorite_category,@favorite) }
      else
        format.html { render :edit }
        format.json { render json: @favorite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favorites/1
  # DELETE /favorites/1.json
  def destroy
    @favorite.destroy
    respond_to do |format|
      format.html { redirect_to favorite_category_favorites_url(@favorite_category), notice: 'Favorite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favorite_category
      @favorite_category = current_user.favorite_categories.find(params[:favorite_category_id])
    end

    def set_favorite
      @favorite = @favorite_category.favorites.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favorite_params
      params.require(:favorite).permit(:name, :author, :year, :url, :details, :destription)
    end
end

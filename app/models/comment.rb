# == Schema Information
#
# Table name: comments
#
#  id                   :integer          not null, primary key
#  message              :string
#  favorite_category_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_comments_on_favorite_category_id  (favorite_category_id)
#

class Comment < ApplicationRecord
  belongs_to :favorite_category
end

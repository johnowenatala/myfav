class CreateFavorites < ActiveRecord::Migration[5.2]
  def change
    create_table :favorites do |t|
      t.references :favorite_category, foreign_key: true, null: false
      t.string :name, null: false
      t.string :author
      t.integer :year
      t.string :url
      t.string :details
      t.string :destription

      t.timestamps
    end
  end
end

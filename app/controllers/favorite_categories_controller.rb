class FavoriteCategoriesController < ApplicationController

  before_action :authenticate_user!
  before_action :set_favorite_category, only: [:show, :edit, :update, :destroy]

  # GET /favorite_categories
  # GET /favorite_categories.json
  def index
    @favorite_categories = current_user.favorite_categories.ordered
  end

  # GET /favorite_categories/1
  # GET /favorite_categories/1.json
  def show
  end

  # GET /favorite_categories/new
  def new
    @favorite_category = current_user.favorite_categories.build
  end

  # GET /favorite_categories/1/edit
  def edit
  end

  # POST /favorite_categories
  # POST /favorite_categories.json
  def create
    @favorite_category = current_user.favorite_categories.build(favorite_category_params)

    respond_to do |format|
      if @favorite_category.save
        format.html { redirect_to @favorite_category, notice: 'Favorite category was successfully created.' }
        format.json { render :show, status: :created, location: @favorite_category }
      else
        format.html { render :new }
        format.json { render json: @favorite_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /favorite_categories/1
  # PATCH/PUT /favorite_categories/1.json
  def update
    respond_to do |format|
      if @favorite_category.update(favorite_category_params)
        format.html { redirect_to @favorite_category, notice: 'Favorite category was successfully updated.' }
        format.json { render :show, status: :ok, location: @favorite_category }
      else
        format.html { render :edit }
        format.json { render json: @favorite_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /favorite_categories/1
  # DELETE /favorite_categories/1.json
  def destroy
    @favorite_category.destroy
    respond_to do |format|
      format.html { redirect_to favorite_categories_url, notice: 'Favorite category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def find_by_order
    order = params.permit(:order)[:order]
    category = FavoriteCategory.where(order: order).first
    if category.present?
      render json: { exists: true, id: category.id }
    else
      render json: { exists: false }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favorite_category
      @favorite_category = current_user.favorite_categories.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def favorite_category_params
      params.require(:favorite_category).permit(:order, :name, :description)
    end
end

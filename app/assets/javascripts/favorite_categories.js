$(document).on('turbolinks:load', function() {

  if ($('#favorite_category_form').length > 0) {
    var form = $('#favorite_category_form');

    form.on('submit',function (ev) {
      /*
      ejercicio 1:
      no permitir que el formulario se envie sin los campos order y name
      */
      // var orderField = $('#favorite_category_order');
      // var nameField = $('#favorite_category_name');
      // if (!orderField.val() || !nameField.val()) {
      //   console.log('falta orden o nombre');
      //   ev.preventDefault();
      // }


      /*
      ejercicio 2:
      si falta order, evitar que se envie formulario y mostrar un mensaje junto al campo order
      si falta nombre, evitar que se envie formulario y mostrar un mensaje junto al campo nombre
      */
      // var orderField = $('#favorite_category_order');
      // var nameField = $('#favorite_category_name');
      // if (!orderField.val()) {
      //   console.log('falta orden');
      //   ev.preventDefault();
      //   orderField.after('<div class="form-text text-danger">Falta el orden</div>');
      // }
      // if (!nameField.val()) {
      //   console.log('falta nombre');
      //   ev.preventDefault();
      //   nameField.after('<div class="form-text text-danger">Falta el nombre</div>');
      // }


      /*
      ejercicio 3:
      ejercicio 2 +
        - si order ya tiene un mensaje de error, no duplicar el mensaje
        - si nombre ya tiene un mensaje de error, no duplicar el mensaje
      */
      // var orderField = $('#favorite_category_order');
      // var nameField = $('#favorite_category_name');
      // if (!orderField.val()) {
      //   console.log('falta orden');
      //   ev.preventDefault();
      //   if ($('#order-error-message').length == 0) {
      //     orderField.after('<div class="form-text text-danger" id="order-error-message">Falta el orden</div>');
      //   }
      // }
      // if (!nameField.val()) {
      //   console.log('falta nombre');
      //   ev.preventDefault();
      //   if ($('#name-error-message').length == 0) {
      //     nameField.after('<div class="form-text text-danger" id="name-error-message">Falta el nombre</div>');
      //   }
      // }


      /*
      ejercicio 4:
      ejercicio 3 +
        - si order tiene valor, quitar mensaje de error
        - si nombre tiene valor, quitar mensaje de error
      */
      // var orderField = $('#favorite_category_order');
      // var nameField = $('#favorite_category_name');
      // if (!orderField.val()) {
      //   console.log('falta orden');
      //   ev.preventDefault();
      //   if ($('#order-error-message').length == 0) {
      //     orderField.after('<div class="form-text text-danger" id="order-error-message">Falta el orden</div>');
      //   }
      // } else {
      //   $('#order-error-message').remove();
      // }
      // if (!nameField.val()) {
      //   console.log('falta nombre');
      //   ev.preventDefault();
      //   if ($('#name-error-message').length == 0) {
      //     nameField.after('<div class="form-text text-danger" id="name-error-message">Falta el nombre</div>');
      //   }
      // } else {
      //   $('#name-error-message').remove();
      // }


      /*
      ejercicio 5:
        al cambiar el campo de order, ajax para verificar si se puede usar. si no se puede, mensaje de error.
        El request ajax debe ser a /find_by_order y como parámetro se le debe pasar order
        con el valor de order que se está buscando.
        De acuerdo a la respuesta, se puede usar si la respuesta dice exists == false o
        si el id que entrega es el mismo en el campo #category_id
      */

      var orderField = $('#favorite_category_order');
      var nameField = $('#favorite_category_name');
      if (!orderField.val()) {
        console.log('falta orden');
        ev.preventDefault();
        if ($('#order-error-message').length == 0) {
          orderField.after('<div class="form-text text-danger" id="order-error-message">Falta el orden</div>');
        }
      } else {
        $('#order-error-message').remove();
      }
      if (!nameField.val()) {
        console.log('falta nombre');
        ev.preventDefault();
        if ($('#name-error-message').length == 0) {
          nameField.after('<div class="form-text text-danger" id="name-error-message">Falta el nombre</div>');
        }
      } else {
        $('#name-error-message').remove();
      }


    });

    var orderField = $('#favorite_category_order');
    orderField.on('change', function (ev) {
      console.log('verificando orden');
      $.ajax('/find_by_order',
          {
            data: {order: orderField.val()}
          }
      ).then(function(result) {

        if (result['exists'] == true && result['id'] != $('#category_id').val()) {
          console.log('el orden ya esta asignado a otra categoria');
          ev.preventDefault();
          if ($('#order-error-message').length == 0) {
            orderField.after('<div class="form-text text-danger" id="order-error-message">Otra categoría está usando este orden</div>');
          }
        } else {
          console.log('todo bien con el orden');
          $('#order-error-message').remove();
        }

      });
    });

  }
});